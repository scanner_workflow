#!/usr/bin/env python3

from __future__ import annotations

import click
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler, FileSystemEvent
from pathlib import Path
from typing import Union
from filelock import Timeout, FileLock
import subprocess
from logging import error, info, debug, warning
import re
from time import sleep


class ScannerWorkflowEvent(FileSystemEventHandler):
    """Subclass of FileSystemEventHandler to handle OCRing PDFs"""

    scanner_workflow: ScannerWorkflow

    def __init__(self, scanner_workflow: ScannerWorkflow):
        super().__init__()
        self.scanner_workflow = scanner_workflow
        if not self.scanner_workflow:
            raise Exception("No scanner_workflow passed to ScannerWorkflowEvent")

    def on_any_event(self, event: FileSystemEvent):
        if event.is_directory:
            return
        if not event.src_path.endswith(".pdf"):
            return
        pdf_file = Path(event.src_path)
        if pdf_file.exists():
            self.scanner_workflow.process_pdf(pdf_file)


class ScannerWorkflow:
    base_dir = None
    failure_dir = None
    output_dir = None
    lock_file = None
    input_dir = None
    process_dir = None
    ocrmypdf_opts = ["-r", "-q", "--deskew", "--clean"]

    def __init__(
        self,
        base_dir: Union[Path, str] = ".",
        input_dir: Union[Path, str] = "input",
        output_dir: Union[Path, str] = "output",
        failure_dir: Union[Path, str] = "failure",
        process_dir: Union[Path, str] = "process",
        lock_file: Union[Path, str] = ".lock",
    ):
        def concat_if_not_abs(dir1: Path, dir2: Path):
            if dir2.is_absolute():
                return dir2
            else:
                return dir1 / dir2

        super().__init__()
        self.base_dir = Path(base_dir)
        self.input_dir = concat_if_not_abs(self.base_dir, Path(input_dir))
        self.output_dir = concat_if_not_abs(self.base_dir, Path(output_dir))
        self.failure_dir = concat_if_not_abs(self.base_dir, Path(failure_dir))
        self.process_dir = concat_if_not_abs(self.base_dir, Path(process_dir))
        self.lock_file = concat_if_not_abs(self.base_dir, Path(lock_file))
        self.lock = FileLock(self.lock_file)
        self.base_dir.mkdir(parents=True, exist_ok=True)
        self.input_dir.mkdir(parents=True, exist_ok=True)
        self.failure_dir.mkdir(parents=True, exist_ok=True)
        self.process_dir.mkdir(parents=True, exist_ok=True)
        self.output_dir.mkdir(parents=True, exist_ok=True)

    def calculate_name(self, name: str):
        res = re.match(
            r"(?P<scanner>[^_]+)_(?P<month>\d{2})(?P<day>\d{2})(?P<year>\d{4})_"
            r"(?P<time>\d+)_(?P<counter>\d+)\.pdf",
            str(name),
        )
        if res:
            name = (
                f"{res.group('scanner')}_"
                f"{res.group('year')}{res.group('month')}{res.group('day')}_"
                f"{res.group('time')}_{res.group('counter')}.pdf"
            )
        return name

    def pdf_file_path(self, name: str):
        res = re.match(
            r"(?P<scanner>[^_]+)_(?P<month>\d{2})(?P<day>\d{2})(?P<year>\d{4})_"
            r"(?P<time>\d+)_(?P<counter>\d+)\.pdf",
            str(name),
        )
        if res:
            return f"{res.group('year')}"
        return ""

    def process_pdf(self, pdf_file: Union[Path, str]):
        """Process a single PDF."""
        pdf_file = Path(pdf_file)
        orig_pdf = pdf_file
        # check that the pdf is good, otherwise wait to see if it
        # might become good
        pdf_good = False
        for i in range(1, 10):
            check = subprocess.run(["qpdf", "--check", pdf_file])
            if check.returncode == 0:
                pdf_good = True
                break
            file_size = pdf_file.stat().st_size
            # sleep in a loop for 10 seconds if the file size is still
            # increasing
            while True:
                sleep(10)
                new_size = pdf_file.stat().st_size
                if new_size > file_size:
                    file_size = new_size
                else:
                    break
        if not pdf_good:
            error(f"PDF was not good, skipping {orig_pdf} for now")
            return

        # move to the processing directory
        output_path = self.pdf_file_path(pdf_file.name)
        pdf_file = pdf_file.rename(
            self.process_dir / self.calculate_name(pdf_file.name)
        )
        (self.output_dir / output_path).mkdir(parents=True, exist_ok=True)
        output_file = self.output_dir / output_path / pdf_file.name
        res = subprocess.run(["ocrmypdf", *self.ocrmypdf_opts, pdf_file, output_file])
        if res.returncode != 0:
            error(
                f"Unable to properly OCR pdf {orig_pdf} into {output_file}: {res.stdout} {res.stderr}"
            )
            return
        pdf_file.unlink()
        info(f"Processed {orig_pdf} into {output_file}")

    def event_loop(self):
        """Main event loop; called from the command line."""
        ev = ScannerWorkflowEvent(scanner_workflow=self)
        observer = Observer()
        observer.schedule(ev, self.input_dir, recursive=True)
        observer.start()
        # process any PDFs in input_dir
        for file in self.input_dir.iterdir():
            self.process_pdf(file)
        try:
            while observer.is_alive():
                observer.join(1)
        finally:
            observer.stop()
            observer.join()


@click.command()
@click.option(
    "-i",
    "--input-dir",
    default="input",
    help="Directory to look for incoming PDFs",
)
@click.option(
    "-p",
    "--process-dir",
    default="process",
    help="Directory to store PDFs being processed",
)
@click.option(
    "-o",
    "--output-dir",
    default="output",
    help="Directory to output OCRed PDFs",
)
@click.option(
    "-f",
    "--failure-dir",
    default="failure",
    help="Directory to store failed PDFs",
)
@click.option(
    "-b",
    "--base-dir",
    default=".",
    help="Base directory",
)
@click.option(
    "-l",
    "--lock-file",
    default=".lock",
    help="Lock file to ensure only one instance is running",
)
def cli(input_dir, process_dir, output_dir, failure_dir, base_dir, lock_file):
    """OCR scanner output and save in directory"""
    sw = ScannerWorkflow(
        input_dir=input_dir,
        process_dir=process_dir,
        output_dir=output_dir,
        failure_dir=failure_dir,
        base_dir=base_dir,
        lock_file=lock_file,
    )
    try:
        with sw.lock.acquire(timeout=10):
            sw.event_loop()
    except Timeout:
        print("Another instance holds the lock")
        exit(1)


cli()
